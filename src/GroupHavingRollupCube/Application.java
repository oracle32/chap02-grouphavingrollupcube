package GroupHavingRollupCube;

public class Application {

	
	/*
	 * -- GROUP BY 와 HAVING 그리고 ORDER BY
-- SELECT 구문 맨 마지막에 위치한다. 실행 순서도 맨 마지막에 실행된다.
-- ORDER BY 절 : SELECT 한 컬럼을 가지고 정렬을 할 때 사용한다.



  5 : SELECT 컬럼명 AS 별칭, 계산식 ,함수식
  1 : FROM 참조할 테이블명
  2 : WHERE 컬럼명 | 함수식 비교연산자 비교값
  3 : GROUP BY 그룹을 묶을 컬럼명
  4 : HAVING 그룹함수식 비교연산자 비교값
  6 : ORDER BY 컬럼명 | 별칭 | 컬럼순번 정렬방식 [NULLS FRIST | LAST]


GROUP BY 절에서 [ 조건을 설정할 때 HAVING 절을 이용한다]
--HAVING절 : 그룹함수로 구해올 그룹에 대해 조건을 설정할 때 사용
-- HAVING 컬럼명 | 함수식 비교 연산자 비교값 



-- 집계 함수
-- ROLLUP 함수 : 그룹별로 중간 집계 처리를 하는 함수
-- GROUP BY 절에서만 사용하는 함수
-- 그룹별로 묶여진 값에 대한 중간집계와 총집계를 구할때 사용 한다.
-- 그룹별로 계산된 결과값들에 대한 총 집계가 자동으로 추가된다.
SELECT
       DEPT_CODE
     , JOB_CODE
     , SUM(SALARY)
  FROM EMPLOYEE
 GROUP BY ROLLUP(DEPT_CODE, JOB_CODE)
 ORDER BY 1;


-- 그룹으로 지정된 모든 그룹에 대한 집계와 총 합계를 구하는 함수
SELECT
       DEPT_CODE
     , JOB_CODE
     , SUM(SALARY)
  FROM EMPLOYEE
 GROUP BY CUBE(DEPT_CODE, JOB_CODE)
 ORDER BY 1;


GROUPING 함수 : ROLLUP이나 CUBE에 의한 산출몰이 
인자로 전달받은 컬럼 집합의 산출몰이 0을 반환/ 아니면 1을 반환

SELECT 절에
         GROUPING(DEPT_CODE) " 부서별 그룹 묶임"
         GROUPING(JOB_CODE) "직급별 묶인상태"


- SET OPERATION 
여러개의 SELECT 결과물을 하나의 쿼리로 만드는 연산자
UNION : 여러개의 쿼리를 하나로 합치는 연산자 (중복된거 제거)
UNION ALL :  모든영역을 표시 , (중복된거 포함)
INTERSECT : 공통적인 부분만 추출한다. (교집합) 
MINUS : 공통적인부분 제외 나머지 (차집합)

컬럼의 숫자를 맞춰줘야 쓸수있다.
쓸 값이 없으면 빈값 '  ' 이라도 써주면서 맞춰줘야 한다.


	 * 
	 * 
	 * */
}
